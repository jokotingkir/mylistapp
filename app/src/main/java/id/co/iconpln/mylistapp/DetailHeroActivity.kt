package id.co.iconpln.mylistapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_hero.*

class DetailHeroActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_HERO = "EXTRA_HERO"
    }

    private lateinit var hero: Hero

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hero)

        setUpActionBar()
        initIntentExtras()
        displayHeroDetail()
    }

    private fun initIntentExtras() {
        hero = intent.getParcelableExtra(EXTRA_HERO)
    }

    private fun setUpActionBar() {
        supportActionBar?.title = hero.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun displayHeroDetail() {
        tvHeroDetailName.text = hero.name
        tvHeroDetailDesc.text = hero.description
        Glide.with(this).load(hero.photo).apply(
            RequestOptions().centerCrop().placeholder(R.drawable.ic_launcher_foreground).error(R.drawable.ic_launcher_foreground)
        ).into(ivHeroDetailImage)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> false
    }
}
